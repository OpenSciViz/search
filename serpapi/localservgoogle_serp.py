import os
import serpapi

client = serpapi.Client(api_key=os.getenv("API_KEY"))
results = client.search({
    'engine': 'google_local_services',
    'q': 'electrician',
    'data_cid': '6745062158417646970',
})
