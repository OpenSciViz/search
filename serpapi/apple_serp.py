import os
import serpapi

client = serpapi.Client(api_key=os.getenv("API_KEY"))
results = client.search({
    'engine': 'apple_app_store',
    'term': 'coffee',
})
