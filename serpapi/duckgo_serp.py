import os
import serpapi

client = serpapi.Client(api_key=os.getenv("API_KEY"))
results = client.search({
    'engine': 'duckduckgo',
    'q': 'coffee',
})
